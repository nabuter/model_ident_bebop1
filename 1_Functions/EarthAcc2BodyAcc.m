function [ACC_B] = EarthAcc2BodyAcc(TIME, ATT, ACC_E)
%% calculate body acceleration based on earth acceleration and attitude
%% INPUT : ACC_E, ATT
%% OUTPUT: ACC_B
ACC_B = zeros(length(TIME),3);
global g;
for i = 1:length(TIME)-1
    phi   = degtorad( ATT(i,1) );
    theta = degtorad( ATT(i,2) );
    psi   = degtorad( ATT(i,3) );
    
    R_E_B = [cos(theta)*cos(psi) cos(theta)*sin(psi) -sin(theta);...
        sin(phi)*sin(theta)*cos(psi)-cos(phi)*sin(psi)...
        sin(phi)*sin(theta)*sin(psi)+cos(phi)*cos(psi) sin(phi)*cos(theta);...
        cos(phi)*sin(theta)*cos(psi)+sin(phi)*sin(psi)...
        cos(phi)*sin(theta)*sin(psi)-sin(phi)*cos(psi) cos(phi)*cos(theta)];
    
    ACC_B(i,:) = [R_E_B*[ACC_E(i,:)'-[0 0 g]']]';

end