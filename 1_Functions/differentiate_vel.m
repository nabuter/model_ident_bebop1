function [ACC_E] =differentiate_vel(VEL_E, TIME)

%% calculate earth-frame acceleration from earth-frame velocity
%% Input : OT.VEL_E, OT.TIME
%% Output: OT_a.ACC_E

v_z_smoothed = (VEL_E(:,3));
v_x_smoothed = (VEL_E(:,1));
v_y_smoothed = (VEL_E(:,2));
a_z_raw = diff(v_z_smoothed)./diff(TIME);
a_x_raw = diff(v_x_smoothed)./diff(TIME);
a_y_raw = diff(v_y_smoothed)./diff(TIME);
ACC_E = [(a_x_raw) (a_y_raw) (a_z_raw)];
end