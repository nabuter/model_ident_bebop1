function [RPM_filtered] = filter_RPM_data(RPM_full)
RPM_filtered=RPM_full;
RPM_filtered.act(:,1) =smooth(RPM_full.act(:,1),10);
RPM_filtered.act(:,2) =smooth(RPM_full.act(:,2),10);
RPM_filtered.act(:,3) =smooth(RPM_full.act(:,3),10);
RPM_filtered.act(:,4) =smooth(RPM_full.act(:,4),10);


end