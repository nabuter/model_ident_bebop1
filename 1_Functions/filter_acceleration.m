function [ACC_E_filtered] = filter_acceleration(ACC_E);

ACC_E_filtered(:,1) = smooth(ACC_E(:,1),10);
ACC_E_filtered(:,2) = smooth(ACC_E(:,2),10);
ACC_E_filtered(:,3) = smooth(ACC_E(:,3),10);

end
