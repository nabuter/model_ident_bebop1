function [VEL_E_filtered, VEL_B_filtered] = filter_velocity(VEL_E, VEL_B)

VEL_E_filtered(:,3) = smooth(VEL_E(:,3),20);
VEL_E_filtered(:,1) = smooth(VEL_E(:,1),10);
VEL_E_filtered(:,2) = smooth(VEL_E(:,2),10);

VEL_B_filtered(:,3) = smooth(VEL_B(:,3),10);
VEL_B_filtered(:,1) = smooth(VEL_B(:,1),10);
VEL_B_filtered(:,2) = smooth(VEL_B(:,2),10);

end