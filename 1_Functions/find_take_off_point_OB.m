
function [i, take_off_time] = find_take_off_point_OB( OB )

altitude_before_take_off = mean(OB.PosNED(1:200,3));

for i = 1:length(OB.time)
    if abs(OB.PosNED(i,3)-altitude_before_take_off) > 0.02
        break;
    end
end

take_off_time = OB.time(i);

end