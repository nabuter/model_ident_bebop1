function [ i, take_off_time] = find_take_off_point_OT( OT )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

altitude_before_take_off = mean(OT.Z(1:200));

for i = 1:length(OT.TIME)
    if abs(OT.Z(i)-altitude_before_take_off) > 0.02
        break;
    end
end

take_off_time = OT.TIME(i);

end