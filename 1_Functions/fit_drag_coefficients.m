function[]=fit_drag_coefficients(vel_B, acc_B, t)
%% Remove NAN elements
nan_list = any(isnan(vel_B), 2) + any(isnan(acc_B), 2);
vel_B(nan_list >= 1, :) = [];
acc_B(nan_list >= 1, :) = [];
%% Load Data
vel_B_x = vel_B(:,1);
vel_B_y = vel_B(:,2);
vel_B_z = vel_B(:,3);

acc_B_x = acc_B(:,1);
acc_B_y = acc_B(:,2);
acc_B_z = acc_B(:,3);

%% Least Square fitting Linear Model 
model_Cx = polyfit(vel_B_x,acc_B_x, 1);
model_Cy = polyfit(vel_B_y,acc_B_y, 1);
model_Cz = polyfit(vel_B_z,acc_B_z, 1);
acc_B_x_regression = polyval(model_Cx,vel_B_x);
acc_B_y_regression = polyval(model_Cy,vel_B_y);
acc_B_z_regression = polyval(model_Cz,vel_B_z);
disp(['Linear Regression Drag Model:'])
disp(['C_x -> slope: ', num2str(model_Cx(1)), '; offset: ',num2str(model_Cx(2))])
disp(['C_y -> slope: ', num2str(model_Cy(1)), '; offset: ',num2str(model_Cy(2))])
disp(['C_z -> slope: ', num2str(model_Cz(1)), '; offset: ',num2str(model_Cz(2))])

%% Generate acceleration model using drag constant from Christophes paper
Cx = -0.1938; % slope in N/(m/s)
global mass;  % mass in kg
slope = Cx/mass; %slope in (1/s)
acc_model_christophe_x = vel_B_x*slope;
acc_model_christophe_y = vel_B_y*slope;
% 
% %% Plot Body Velocity vs time
% figure(1)
% subplot(3,1,1)
% plot(t, vel_B_x);
% xlabel('time[s]'); ylabel('vel_{B,x}[m/s]');
% subplot(3,1,2)
% plot(t, vel_B_y);
% xlabel('time[s]'); ylabel('vel_{B,y}[m/s]');
% subplot(3,1,3)
% plot(t, vel_B_z);
% xlabel('time[s]'); ylabel('vel_{B,z}[m/s]');
% 
% %% Plot Body Acceleration vs time
% figure(2)
% subplot(3,1,1)
% plot(t, acc_B_x);
% xlabel('time[s]'); ylabel('acc_{B,x}[m/s]')
% subplot(3,1,2)
% plot(t, acc_B_y);
% xlabel('time[s]'); ylabel('acc_{B,y}[m/s]')
% subplot(3,1,3)
% plot(t, acc_B_z);
% xlabel('time[s]'); ylabel('acc_{B,z}[m/s]')

%% Define TIME Range to be plotted
t_sample_start = 15;
t_sample_end   = 50;

%% Plot Body Acceleration vs Body Velocities in X-dir
first_sample = round(t_sample_start/t(end)*length(vel_B_x));
last_sample = round(t_sample_end/t(end)*length(vel_B_x));

figure(3)
plot(vel_B_x(first_sample:last_sample), acc_B_x(first_sample:last_sample),'.b');
hold on
plot(vel_B_x(first_sample:last_sample), acc_model_christophe_x(first_sample:last_sample),'.r');
hold on
plot(vel_B_x(first_sample:last_sample), acc_B_x_regression(first_sample:last_sample),'.g');
hold off
xlabel('vel_B_x[m/s]'); ylabel('acc_{B,x}[m/s2]');
legend('Experiment Data','Christophes Linear Model','Linear Regression')

%% Plot Body Acceleration vs Body Velocities in Y-dir
first_sample = round(t_sample_start/t(end)*length(vel_B_y));
last_sample = round(t_sample_end/t(end)*length(vel_B_y));

figure(4) 
plot(vel_B_y(first_sample:last_sample), acc_B_y(first_sample:last_sample),'.b');
hold on
plot(vel_B_y(first_sample:last_sample), acc_model_christophe_y(first_sample:last_sample),'.g');
hold on
plot(vel_B_y(first_sample:last_sample), acc_B_y_regression(first_sample:last_sample),'.r');
hold off 
xlabel('vel_B_y[m/s]'); ylabel('acc_{B,y}[m/s2]');
legend('Experiment Data','Christophes Linear Model', 'Linear Regression')

%% Plot Body Acceleration vs Body Velocities in Z-dir
first_sample = round(t_sample_start/t(end)*length(vel_B_z));
last_sample = round(t_sample_end/t(end)*length(vel_B_z));

figure(5) 
plot(vel_B_z(first_sample:last_sample), acc_B_z(first_sample:last_sample),'.b');
hold on
plot(vel_B_z(first_sample:last_sample), acc_B_z_regression(first_sample:last_sample),'.r');
hold off 
xlabel('vel_B_z[m/s]'); ylabel('acc_{B,z}[m/s2]');
legend('Experiment Data', 'Linear Regression')

end