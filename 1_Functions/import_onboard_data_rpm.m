function [RPM]= import_onboard_data_rpm(name)
data = csvread([ name '.csv'],0,0);

RPM.time = data(:,2);
RPM.PosNED = data(:,15:17);
RPM.com = data(:,27:30);
RPM.act = data(:,31:34);


end