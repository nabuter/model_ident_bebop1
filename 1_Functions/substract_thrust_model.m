function [ACC_B_MinusThrust] = substract_thrust_model(ACC_B,time_acc, RPM);
global mass;
RPS = RPM.act/60;
%slope = 6.68542140e-05; %Slope in N/(rotations/s)^2
%slope = 6.68542140e-05 * 0.89; %Slope in N/(rotations/s)^2
%thrust = RPS.^2 * slope;
bias=0.885;
slope = 6.68542140e-05 *bias;%Slope in N/(rotations/s)^2
thrust =  RPS.^2 * slope;


total_thrust = sum(thrust, 2);

total_thrust_ = interp1(RPM.time,total_thrust,time_acc);

ACC_B_MinusThrust = ACC_B;
ACC_B_MinusThrust(:,3)=ACC_B(:,3)+total_thrust_/mass;
end