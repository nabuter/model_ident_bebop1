function [ATT_onboard] = import_onboard_data_ATT(name);
data = csvread([ name '.csv'],0,0);

take_off_pointer_OB=1437;

ATT_onboard(:,1) =data(take_off_pointer_OB:end,12)*57.3;%phi
ATT_onboard(:,2) =data(take_off_pointer_OB:end,13)*57.3;%theta
ATT_onboard(:,3) =data(take_off_pointer_OB:end,14)*57.3;%psi

end