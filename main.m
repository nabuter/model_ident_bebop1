clear;clc;close all;
[path, ~, ~] = fileparts(mfilename('fullpath')); addpath(genpath(path));

global g;    g    = 9.81; 
global mass; mass = 0.415;

%% Convert raw experimental data to Data in Earth and body frame
[OT_full] = import_optitrack_data('2017-03-01 04.47.30 PM_flight_4',{'Rigid Body 3'});
%% Import RPM from Onboard data
RPM_full = import_onboard_data_rpm('5deg_10circles');
RPM_filtered = filter_RPM_data(RPM_full);

%% Get rid of meaningless samples(before takeoff)
[OT_cut, RPM_cut] = cut_data(OT_full, RPM_filtered);
%This script writes: POS_E, VEL_E, ATT, VEL_B, PQR_B, TIME

%% Filter Velocity in Earth Frame
[OT_cut.VEL_E_filtered, OT_cut.VEL_B_filtered] = filter_velocity(OT_cut.VEL_E, OT_cut.VEL_B);

%% Differentiate in Earth Frame Velocity to Acceleration
OT_cut.ACC_E = differentiate_vel(OT_cut.VEL_E_filtered, OT_cut.TIME);

%% Filter Acceleration in Earth Frame
OT_cut.ACC_E_filtered = filter_acceleration(OT_cut.ACC_E);

%% Convert Earth Frame Acceleration to Body Frame Acceleration
[OT_cut.ACC_B] = EarthAcc2BodyAcc(OT_cut.TIME, OT_cut.ATT, OT_cut.ACC_E_filtered);

%% Substract Thrust Model from Z-acceleration in Body Frame
OT_cut.ACC_B_MinusThrust = substract_thrust_model(OT_cut.ACC_B,OT_cut.TIME, RPM_cut);

%% Linear Regression Fitting Linear Drag Model
fit_drag_coefficients(OT_cut.VEL_B_filtered, OT_cut.ACC_B_MinusThrust, OT_cut.TIME );